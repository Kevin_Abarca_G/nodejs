mapboxgl.accessToken = 'pk.eyJ1Ijoia2V2aW41MzI1MzI0NSIsImEiOiJja2ViY3pvb3MwN241MnhvYWxoZWxhcmo2In0.s-7s7RKp3pwnYSr28vhZcQ';
var map = new mapboxgl.Map({
container: 'mapid',
style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
center: [-70.654059, -33.442715], // starting position [lng, lat]
zoom: 14 // starting zoom
});

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            new mapboxgl.Marker()
            .setPopup(new mapboxgl.Popup().setHTML(`<h6>Datos Bicicletas</h6><p>Id: ${bici.id}</p><p>Color: ${bici.color}</p><p>Modelo: ${bici.modelo}</p><p>LAT: ${bici.ubicacion[1]} | LNG: ${bici.ubicacion[0]}</p>`))
            .setLngLat(bici.ubicacion).addTo(map);
        });
    }
})

