var Bicicleta = function (id, color, modelo, ubicacion){
    // cuando usamos el new Bicicleta estos se usan como constructor.
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return 'id: '+this.id+' | color: '+this.color+' | modelo: '+this.modelo+' | ubicacion: '+this.ubicacion;
}

// array para guardar bicis
Bicicleta.allBicis = [];

//funcion agregar bicicleta en array
Bicicleta.add = function (aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if(aBici)
      return aBici;
    else
      throw new Error(`No existe una bicicleta con el Id: ${aBiciId}`);
}

Bicicleta.removeById = function(aBiciId){
    for(var i = 0; i < Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

//agregar bicicletas.
var a = new Bicicleta(1, 'rojo', 'urbana', [-70.655232, -33.444250]);
var b = new Bicicleta(2, 'blanca', 'urbana', [-70.655600, -33.444600]);
Bicicleta.add(a);
Bicicleta.add(b);

// usado para que lo puedan utilizar otros archivos
module.exports = Bicicleta;