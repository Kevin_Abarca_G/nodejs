var Bicicleta = require('../models/bicicletas');

exports.bicicletas_list = function(req, res) {

    res.render('bicicletas/index', {bicis:Bicicleta.allBicis});
}

exports.bicicletas_create_get = function(req, res) {
    res.render('bicicletas/create');
}
exports.bicicletas_create_post = function(req, res) {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lng, req.body.lat];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');
}

exports.bicicletas_delete_post = function(req, res){
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}

exports.bicicletas_update_get = function(req, res) {
    var bici = Bicicleta.findById(req.params.id);

    res.render('bicicletas/update', {bici});
}
exports.bicicletas_update_post = function(req, res) {
    var bici = Bicicleta.findById(req.params.id);
    console.log(req.params.id, bici);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lng, req.body.lat];
    res.redirect('/bicicletas');
}